var gulp = require('gulp');
var browserify = require('browserify');// JSX TRANSFORMER
var babelify = require('babelify');//Browserify 와 babelify를 조합해서 ES6와 react.js의 JSX 문법을 사용
var source = require('vinyl-source-stream');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('js', function(){
  return browserify('./src/js_real/app.js')// 내가 코드를 작성할 경로
  .transform(babelify.configure({presets: ["es2015", "react"]}))//babelify 를 단독으로 사용불가
  .bundle()
  .pipe(source('myreact.js'))// 변환되어질 JS 의 파일명
  .pipe(gulp.dest('./src/js/'))// 내가 작성한 코드가 뿌려질 경로
})


gulp.task('bs', function () {
    browserSync({
        files: ['*.html', '*.php'],
        proxy: 'localhost:80',
        open: 'external',
        logPrefix: "bs"
    });

    gulp.watch('src/js_real/*.js', ['js']);
    gulp.watch(['src/*.html', 'src/js/*.js']).on('change', browserSync.reload);
});