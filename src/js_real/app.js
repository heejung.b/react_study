var React = require('react');
var ReactDOM = require('react-dom');

var TodoBox = React.createClass({
    loadTodoListFromServer: function(){
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data){
                this.setState({data:data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    handleTodoSubmit: function(todo){
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            type: 'POST',
            data: todo,
            success: function(data){
                this.setState({data:data});
            }.bind(this),
            error: function(xhr, status, err){
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    getInitialState: function(){
        return{data:[]}
    },
    componentDidMount: function(){
        this.loadTodoListFromServer();
        setInterval(this.loadTodoListFromServer, this.props.pollInterval);
    },
  render: function() {
    return (
        <div>
            <TodoList data={this.state.data}/>
            <TodoForm onTodoSubmit={this.handleTodoSubmit}/>
        </div>
    );
  }
});
var TodoList = React.createClass({
    render: function(){
            var TodoItem = this.props.data.map(function(todo,index){
                return (
                    <li key={index}>
                        <input type="checkbox" id={"chk"+index} className="blind chk_box" defaultChecked={todo.done}/>
                        <label htmlFor={"chk"+index} className="chk_label fa fa-check-circle-o">{todo.things}</label>
                        <button type="button" className="btn_del fa fa-minus-circle"><span className="blind">삭제</span></button>
                    </li>
                );
            });
            return (
                <ol className='wrap_list'>
                    {TodoItem}
                </ol>
            );
    }
});

var TodoForm = React.createClass({
    handleSubmit: function(e){
        e.preventDefault();
        var things = this.refs.things.value.trim();
        if(!things){
            return;
        }
        this.props.onTodoSubmit({things:things});
        this.refs.things.value = '';
        return;
    },
    render:function(){
        return(
            <form className="wrap_write" onSubmit={this.handleSubmit}>
                <input type="text" ref="things"/>
                <input type="submit" className="btn_submit" value="TODO"/>
            </form>
        )
    }
})
ReactDOM.render(
  <TodoBox url="../src/todo.json" pillInterval={2000}/>,
  document.getElementById('container')
);
